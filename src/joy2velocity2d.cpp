﻿// -*- C++ -*-
/*!
 * @file  joy2velocity2d.cpp
 * @brief convert joy to timed velocity2d
 * @date $Date$
 *
 * $Id$
 */

#include "joy2velocity2d.h"

// Module specification
// <rtc-template block="module_spec">
static const char* joy2velocity2d_spec[] =
  {
    "implementation_id", "joy2velocity2d",
    "type_name",         "joy2velocity2d",
    "description",       "convert joy to timed velocity2d",
    "version",           "1.0.0",
    "vendor",            "NUT Kimuralab Keitaro Takeuchi",
    "category",          "Converter",
    "activity_type",     "PERIODIC",
    "kind",              "DataFlowComponent",
    "max_instance",      "1",
    "language",          "C++",
    "lang_type",         "compile",
    // Configuration variables
    "conf.default.vxaxis", "1",
    "conf.default.vyaxis", "0",
    "conf.default.vaaxis", "3",
    "conf.default.vxSignReversal", "1",
    "conf.default.vySignReversal", "0",
    "conf.default.vaSignReversal", "0",

    // Widget
    "conf.__widget__.vxaxis", "spin.1",
    "conf.__widget__.vyaxis", "spin.1",
    "conf.__widget__.vaaxis", "spin.1",
    "conf.__widget__.vxSignReversal", "radio",
    "conf.__widget__.vySignReversal", "radio",
    "conf.__widget__.vaSignReversal", "radio",
    // Constraints
    "conf.__constraints__.vxaxis", "0<=x<=10",
    "conf.__constraints__.vyaxis", "0<=x<=10",
    "conf.__constraints__.vaaxis", "0<=x<=10",
    "conf.__constraints__.vxSignReversal", "(0,1)",
    "conf.__constraints__.vySignReversal", "(0,1)",
    "conf.__constraints__.vaSignReversal", "(0,1)",

    "conf.__type__.vxaxis", "int",
    "conf.__type__.vyaxis", "int",
    "conf.__type__.vaaxis", "int",
    "conf.__type__.vxSignReversal", "short",
    "conf.__type__.vySignReversal", "short",
    "conf.__type__.vaSignReversal", "short",

    ""
  };
// </rtc-template>

/*!
 * @brief constructor
 * @param manager Maneger Object
 */
joy2velocity2d::joy2velocity2d(RTC::Manager* manager)
    // <rtc-template block="initializer">
  : RTC::DataFlowComponentBase(manager),
    m_joyaxisIn("joyaxis", m_joyaxis),
    m_velocity2dOut("velocity2d", m_velocity2d)

    // </rtc-template>
{
}

/*!
 * @brief destructor
 */
joy2velocity2d::~joy2velocity2d()
{
}



RTC::ReturnCode_t joy2velocity2d::onInitialize()
{
  // Registration: InPort/OutPort/Service
  // <rtc-template block="registration">
  // Set InPort buffers
  addInPort("joyaxis", m_joyaxisIn);

  // Set OutPort buffer
  addOutPort("velocity2d", m_velocity2dOut);

  // Set service provider to Ports

  // Set service consumers to Ports

  // Set CORBA Service Ports

  // </rtc-template>

  // <rtc-template block="bind_config">
  // Bind variables and configuration variable
  bindParameter("vxaxis", m_vxaxis, "1");
  bindParameter("vyaxis", m_vyaxis, "0");
  bindParameter("vaaxis", m_vaaxis, "3");
  bindParameter("vxSignReversal", m_vxSignReversal, "1");
  bindParameter("vySignReversal", m_vySignReversal, "0");
  bindParameter("vaSignReversal", m_vaSignReversal, "0");
  // </rtc-template>

  return RTC::RTC_OK;
}

/*
RTC::ReturnCode_t joy2velocity2d::onFinalize()
{
  return RTC::RTC_OK;
}
*/


RTC::ReturnCode_t joy2velocity2d::onStartup(RTC::UniqueId ec_id)
{
  return RTC::RTC_OK;
}


RTC::ReturnCode_t joy2velocity2d::onShutdown(RTC::UniqueId ec_id)
{
  return RTC::RTC_OK;
}


RTC::ReturnCode_t joy2velocity2d::onActivated(RTC::UniqueId ec_id)
{
  return RTC::RTC_OK;
}


RTC::ReturnCode_t joy2velocity2d::onDeactivated(RTC::UniqueId ec_id)
{
  return RTC::RTC_OK;
}


RTC::ReturnCode_t joy2velocity2d::onExecute(RTC::UniqueId ec_id)
{
  if(m_joyaxisIn.isNew()) {
    m_joyaxisIn.read();

    m_velocity2d.data.vx = m_joyaxis.data[m_vxaxis] * (1 - 2 * m_vxSignReversal);
    m_velocity2d.data.vy = m_joyaxis.data[m_vyaxis] * (1 - 2 * m_vySignReversal);
    m_velocity2d.data.va = m_joyaxis.data[m_vaaxis] * (1 - 2 * m_vaSignReversal);

    // std::cout << "vx: " << m_velocity2d.data.vx << std::endl
    //   << "vy: " << m_velocity2d.data.vy << std::endl
    //   << "va: " << m_velocity2d.data.va << std::endl;

    m_velocity2dOut.write();
  }

  return RTC::RTC_OK;
}

/*
RTC::ReturnCode_t joy2velocity2d::onAborting(RTC::UniqueId ec_id)
{
  return RTC::RTC_OK;
}
*/

/*
RTC::ReturnCode_t joy2velocity2d::onError(RTC::UniqueId ec_id)
{
  return RTC::RTC_OK;
}
*/

/*
RTC::ReturnCode_t joy2velocity2d::onReset(RTC::UniqueId ec_id)
{
  return RTC::RTC_OK;
}
*/

/*
RTC::ReturnCode_t joy2velocity2d::onStateUpdate(RTC::UniqueId ec_id)
{
  return RTC::RTC_OK;
}
*/

/*
RTC::ReturnCode_t joy2velocity2d::onRateChanged(RTC::UniqueId ec_id)
{
  return RTC::RTC_OK;
}
*/



extern "C"
{

  void joy2velocity2dInit(RTC::Manager* manager)
  {
    coil::Properties profile(joy2velocity2d_spec);
    manager->registerFactory(profile,
                             RTC::Create<joy2velocity2d>,
                             RTC::Delete<joy2velocity2d>);
  }

};


